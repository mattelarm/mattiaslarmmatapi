//
//  MyDetailViewController.m
//  MLarmMatApi
//
//  Created by IT-Högskolan on 2015-03-09.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import "MyDetailViewController.h"

@interface MyDetailViewController ()

@property (nonatomic) UIGravityBehavior *gravity;
@property (nonatomic) UICollisionBehavior *collision;
@property (nonatomic) UIDynamicAnimator *animator;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *numberLabel;
@property (weak, nonatomic) IBOutlet UILabel *kcalLabel;
@property (weak, nonatomic) IBOutlet UILabel *proteinLabel;
@property (weak, nonatomic) IBOutlet UILabel *fatLabel;
@property (weak, nonatomic) IBOutlet UILabel *carbonhydratesLabel;
@property (weak, nonatomic) NSNumber * kcalValue;

@property (weak, nonatomic) IBOutlet UIView *pie;

@property (weak, nonatomic) IBOutlet UILabel *pieProteinLabel;
@property (weak, nonatomic) IBOutlet UILabel *pieCarbsLabel;
@property (weak, nonatomic) IBOutlet UILabel *pieFatLabel;
@property (weak, nonatomic) IBOutlet UIImageView *thumbdown;

@property (weak, nonatomic) NSNumber * carbValue;
@property (weak, nonatomic) NSNumber * fatValue;
@property (weak, nonatomic) NSNumber * proteinValue;
@property (weak, nonatomic) IBOutlet UILabel * kcalText;
@property (weak, nonatomic) IBOutlet UIImageView *thumbUp;

@end

@implementation MyDetailViewController



- (void)viewDidLoad {
    
    NSString *searchString = [NSString stringWithFormat:@"http://www.matapi.se/foodstuff/%@",self.searchNumber];
    NSURL *url = [NSURL URLWithString:searchString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task =
    [session dataTaskWithRequest:request
               completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                   
                   if (error) {
                       NSLog(@"Error in response: %@",error);
                       return;
                   }
                   //NSLog(@"Completed! Data: %@, error: %@", data, error);
                   NSError *parsingError = nil;
                   
                   NSDictionary *root = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parsingError];
                   
                   if (!parsingError) {
                       dispatch_async(dispatch_get_main_queue(), ^{
                           NSDictionary *nutrientValues = root[@"nutrientValues"];
                           if (root.count >0) {
                               
                               self.nameLabel.text = [NSString stringWithFormat:@"Name: %@",[root objectForKey:@"name"]];
                               self.numberLabel.text = [NSString stringWithFormat:@"Number: %@",[root objectForKey:@"number"]];
                               self.kcalLabel.text = [NSString stringWithFormat:@"Kcal: %@",[nutrientValues objectForKey:@"energyKcal"]];
                               self.kcalValue = [nutrientValues objectForKey:@"energyKcal"];
                               
                               self.proteinLabel.text = [NSString stringWithFormat:@"Protein: %@",[nutrientValues objectForKey:@"protein"]];
                               self.proteinValue = [nutrientValues objectForKey:@"protein"];
                               
                               self.fatLabel.text = [NSString stringWithFormat:@"Fat: %@",[nutrientValues objectForKey:@"fat"]];
                               self.fatValue = [nutrientValues objectForKey:@"fat"];
                               
                                self.carbonhydratesLabel.text = [NSString stringWithFormat:@"Carbohydrates: %@",[nutrientValues objectForKey:@"carbohydrates"]];
                              self.carbValue = [nutrientValues objectForKey:@"carbohydrates"];
                               
                               
                               PieLayer* pieLayer = [[PieLayer alloc] init];
                               pieLayer.maxRadius = 35.0f;
                               pieLayer.minRadius = 10.0f;
                               pieLayer.animationDuration = 3.3;
                               pieLayer.frame = CGRectMake(0, 0, self.pie.frame.size.width, self.pie.frame.size.height);
                               [self.pie.layer addSublayer:pieLayer];
                               
                               float pieCarbValue = [self.carbValue floatValue];
                               float pieProteinValue = [self.proteinValue floatValue];
                               float pieFatValue = [self.fatValue floatValue];
                               float pieKcalValue = [self.kcalValue floatValue];
                               
                               [pieLayer addValues:@[[PieElement pieElementWithValue:pieCarbValue color:[UIColor redColor]],
                                                     [PieElement pieElementWithValue:pieProteinValue color:[UIColor blueColor]],
                                                     [PieElement pieElementWithValue:pieFatValue color:[UIColor greenColor]]] animated:YES];
                               
                               UIImage *thumbdown = [UIImage imageNamed:@"thumbdown.png"];
                               
                               if (pieKcalValue >= 300) {
                                   self.kcalText.text = [NSString stringWithFormat:@"High Kcal value (%@) bad for you! ",[nutrientValues objectForKey:@"energyKcal"]];
                                   self.thumbUp.alpha = 1.0;
                                   [self.thumbUp setImage:thumbdown];
                                   
                               } else {
                                   self.kcalText.text = [NSString stringWithFormat:@"Low Kcal value (%@) great for you!",[nutrientValues objectForKey:@"energyKcal"]];
                                   self.thumbUp.alpha = 1.0;
                               }
                               
                               
                           } else {
                               self.nameLabel.text = @"No topics found.";
                           }
                       });
                       
                   } else {
                       NSLog(@"Couldnt't parse json %@", parsingError);
                   }
               }];
    
    [task resume];
    
    
    [self fadeInLables];
    
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)showLabel {
    
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    
    self.gravity = [[UIGravityBehavior alloc] initWithItems:@[self.kcalText]];
    self.gravity.magnitude = 0.4;
    [self.animator addBehavior:self.gravity];
    
    self.collision = [[UICollisionBehavior alloc] initWithItems:@[self.kcalText]];
    self.collision.translatesReferenceBoundsIntoBoundary = YES;
    [self.animator addBehavior:self.collision];
    
    [self.collision addItem:self.kcalText];
    
    
    
}

-(void)fadeInLables {
    self.numberLabel.alpha = 0.0;
    self.nameLabel.alpha = 0.0;
    self.fatLabel.alpha = 0.0;
    self.carbonhydratesLabel.alpha = 0.0;
    self.kcalLabel.alpha = 0.0;
    self.proteinLabel.alpha = 0.0;
    self.kcalText.alpha = 0.0;
    self.pieProteinLabel.alpha = 0.0;
    self.pieCarbsLabel.alpha = 0.0;
    self.pieFatLabel.alpha = 0.0;
    self.thumbUp.alpha = 0.0;
    
    
    [UIView animateWithDuration:2.4
                          delay:0.4
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         self.nameLabel.alpha = 1;
                         self.numberLabel.alpha = 1;
                         self.fatLabel.alpha = 1;
                         self.carbonhydratesLabel.alpha = 1;
                         self.kcalLabel.alpha = 1;
                         self.proteinLabel.alpha = 1;
                         //self.kcalText.alpha = 1;
                         self.pieProteinLabel.alpha = 1;
                         self.pieCarbsLabel.alpha = 1;
                         self.pieFatLabel.alpha = 1;
                     }completion:^(BOOL finished) {
                         [UIView animateWithDuration:1.4 delay:0.2 options:UIViewAnimationOptionCurveEaseIn animations:^{
                             self.kcalText.alpha = 1;
                              [self showLabel];
                         }completion:nil ];
                         
                     }];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
