//
//  MyPieElement.m
//  MLarmMatApi
//
//  Created by IT-Högskolan on 2015-03-16.
//  Copyright (c) 2015 IT-H&#246;gskolan. All rights reserved.
//

#import "MyPieElement.h"

@implementation MyPieElement
- (id)copyWithZone:(NSZone *)zone
{
    MyPieElement *copyElem = [super copyWithZone:zone];
    copyElem.title = self.title;
    
    return copyElem;
}
@end
