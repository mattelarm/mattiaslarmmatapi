//
//  MyTableViewController.h
//  MLarmMatApi
//
//  Created by IT-Högskolan on 2015-03-06.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTableViewController : UITableViewController <UISearchBarDelegate>

@end
