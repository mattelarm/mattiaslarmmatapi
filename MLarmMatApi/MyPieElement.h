//
//  MyPieElement.h
//  MLarmMatApi
//
//  Created by IT-Högskolan on 2015-03-16.
//  Copyright (c) 2015 IT-H&#246;gskolan. All rights reserved.
//

#import "PieElement.h"

@interface MyPieElement : PieElement
@property (nonatomic, strong) NSString* title;


@end
