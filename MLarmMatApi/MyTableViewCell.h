//
//  MyTableViewCell.h
//  MLarmMatApi
//
//  Created by IT-Högskolan on 2015-03-12.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *NameLabel;
@property (weak, nonatomic) IBOutlet UILabel *NumberLabel;

@property (strong, nonatomic) NSNumber *number;

@end
