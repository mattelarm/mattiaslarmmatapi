//
//  MyTableViewCell.m
//  MLarmMatApi
//
//  Created by IT-Högskolan on 2015-03-12.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import "MyTableViewCell.h"

@implementation MyTableViewCell
@synthesize NameLabel = _nameLabel;
@synthesize NumberLabel = _numberLabel;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
