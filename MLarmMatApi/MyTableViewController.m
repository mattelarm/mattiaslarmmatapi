//
//  MyTableViewController.m
//  MLarmMatApi
//
//  Created by IT-Högskolan on 2015-03-06.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import "MyTableViewController.h"
#import "MyDetailViewController.h"
#import "MyTableViewCell.h"

@interface MyTableViewController ()

@property (nonatomic) NSArray *dataArray;
@property (nonatomic) NSArray *searchResult;

@property (nonatomic) NSString *apiSearch;
@property (nonatomic) NSNumber *searchNumber;




@end

@implementation MyTableViewController


- (void)viewDidLoad {
    
    self.apiSearch = @"";
    [self fillTable:self.apiSearch];
    
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(tableView == self.tableView) {
        return self.dataArray.count;
        
    } else {
       return self.searchResult.count;
    }
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MyTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"MyCell" forIndexPath:indexPath];
    
    if(tableView == self.tableView) {
        cell.number = self.dataArray[indexPath.row][@"number"];
        cell.textLabel.text = self.dataArray[indexPath.row][@"name"];
    } else {
        cell.number = self.searchResult[indexPath.row][@"number"];
        cell.textLabel.text = self.searchResult[indexPath.row][@"name"];
    }
    
    
    return cell;
}



-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name contains[c] %@",
                              searchText];
    
    self.apiSearch = searchText;
    self.searchResult = [self.dataArray filteredArrayUsingPredicate:predicate];
    
    [self.tableView reloadData];
}


-(void)fillTable:(NSString *)apiSearch {
    NSString *searchString = [NSString stringWithFormat:@"http://www.matapi.se/foodstuff?query=%@",self.apiSearch];
    NSURL *url = [NSURL URLWithString:searchString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task =
    [session dataTaskWithRequest:request
               completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                   
                   if (error) {
                       NSLog(@"Error in response: %@",error);
                       return;
                   }
                   //NSLog(@"Completed! Data: %@, error: %@", data, error);
                   NSError *parsingError = nil;
                   
                   NSArray *root = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parsingError];
                   if (!parsingError) {
                       
                       dispatch_async(dispatch_get_main_queue(), ^{
                           if (root.count >0) {
                               self.dataArray = root;
                           } else {
                               NSLog(@"No topics found.");
                           }
                           [self.tableView reloadData];
                           
                       });
                       
                   } else {
                       NSLog(@"Couldnt't parse json %@", parsingError);
                   }
               }];
    
    [task resume];
    
}



/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"showDetail"]){
        MyDetailViewController *detailView = [segue destinationViewController];
        MyTableViewCell *cell = sender;
        detailView.searchNumber = cell.number;
    }
}


@end
