//
//  MyDetailViewController.h
//  MLarmMatApi
//
//  Created by IT-Högskolan on 2015-03-09.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MagicPieLayer.h>
#import "MyPieElement.h"

@interface MyDetailViewController : UIViewController

@property (nonatomic) NSNumber *searchNumber;

@end
